	
	
<script>
	$(document).ready(function() {
		//App.init();
		var link = '<?php echo base_url() ?>reportes/';

		funciones = {
			iniciar_reportes : function(){
				funciones.listar_eventos();
				funciones.listar_talleres();
				funciones.listar_paquetes();
				funciones.listar_asistencias();
				funciones.listar_pagos();
			},
			listar_eventos(){
				$.ajax({
					url :  link+'listar_eventos',
				   	data : { },
				   	type : 'POST',
				   	dataType: 'JSON',
				   	success  : function(data){
					   	$('#lista_eventos').empty();
					   	$.each(data, function(index, item) {
					   		$('#lista_eventos').append(
								'<tr>'+
			   					'<td class="text-center">'+item.nombre+'</td>'+
			   					'<td class="text-center">'+item.descripcion+'</td>'+
			   					'<td class="text-center">'+item.tipo+'</td>'+
			   					'<td class="text-center">'+item.lugar+'</td>'+
			   					'<td class="text-center">'+item.fecha+'</td>'+
			   					'<td class="text-center">'+item.expositor+'</td>'+
								'<td class="text-center">'+item.asistencias+'</td>'+
			 	 				'</tr>'
					   		);
					   	});
					}
				});
			},
			listar_talleres(){
				$.ajax({
					url :  link+'listar_talleres',
				   	data : { },
				   	type : 'POST',
				   	dataType: 'JSON',
				   	success  : function(data){
					   	$('#lista_talleres').empty();
					   	$.each(data, function(index, item) {
					   		$('#lista_talleres').append(
								'<tr>'+
			   					'<td class="text-center">'+item.nombre+'</td>'+
			   					'<td class="text-center">'+item.descripcion+'</td>'+
			   					'<td class="text-center">'+item.lugar+'</td>'+
			   					'<td class="text-center">'+item.fecha+'</td>'+
			   					'<td class="text-center">'+item.hora+'</td>'+
			   					'<td class="text-center">'+item.expositor+'</td>'+
								'<td class="text-center">'+item.asistencias+'</td>'+
			 	 				'</tr>'
					   		);
					   	});
					}
				});
			},
			listar_paquetes(){
				$.ajax({
					url :  link+'listar_paquetes',
				   	data : { },
				   	type : 'POST',
				   	dataType: 'JSON',
				   	success  : function(data){
					   	$('#lista_paquetes').empty();
					   	$.each(data, function(index, item) {
					   		$('#lista_paquetes').append(
								'<tr>'+
			   					'<td class="text-center">'+item.nombre+'</td>'+
			   					'<td class="text-center">'+item.contenido+'</td>'+
			   					'<td class="text-center">'+item.precio+'</td>'+
			   					'<td class="text-center">'+item.ventas+'</td>'+
			 	 				'</tr>'
					   		);
					   	});
					}
				});
			},
			listar_asistencias(){
				$.ajax({
					url :  link+'listar_asistencias',
				   	data : { },
				   	type : 'POST',
				   	dataType: 'JSON',
				   	success  : function(data){
					   	$('#lista_asistencias').empty();
					   	$.each(data, function(index, item) {
					   		$('#lista_asistencias').append(
								'<tr>'+
			   					'<td class="text-center">'+item.id_uaa+'</td>'+
			   					'<td class="text-center">'+item.asistencia_eventos+'</td>'+
			   					'<td class="text-center">'+item.asistencia_talleres+'</td>'+
			   					'<td class="text-center">'+item.congreso_completado+'</td>'+
			 	 				'</tr>'
					   		);
					   	});
					}
				});
			},
			listar_pagos(){
				$.ajax({
					url :  link+'listar_pagos',
				   	data : { },
				   	type : 'POST',
				   	dataType: 'JSON',
				   	success  : function(data){
					   	$('#lista_pagos').empty();
					   	$.each(data, function(index, item) {
					   		$('#lista_pagos').append(
								'<tr>'+
			   					'<td class="text-center">'+item.id_uaa+'</td>'+
			   					'<td class="text-center">'+item.costo_paquete+'</td>'+
			   					'<td class="text-center">'+item.monto_pagado+'</td>'+
			   					'<td class="text-center">'+item.monto_deber+'</td>'+
			   					'<td class="text-center">'+item.congreso_pagado+'</td>'+
			 	 				'</tr>'
					   		);
					   	});
					}
				});
			},
		};
		// B O T O N E S

		funciones.<?=$modulo?>();

	});
	</script>
